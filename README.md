# Back-end

This
[repository](https://bitbucket.org/V-Mann_Nick/assessment-backend/src/master/)
provides a RESTful API for data on the funding of various industries.  The
major dependencies it utilizes are:
* [Flask](https://github.com/pallets/flask)
* [Flask-Restplus](https://github.com/noirbizarre/flask-restplus)
* [Mongoengine](https://github.com/MongoEngine/mongoengine)
* [Marshmallow](https://github.com/marshmallow-code/marshmallow)

Link to repository: <https://bitbucket.org/V-Mann_Nick/assessment-backend/>

## Interactive Documentation

Flask-Restplus uses [Swagger-UI](https://github.com/swagger-api/swagger-ui) to
generate Documentation for the API. There you can also try out the different
endpoints or copy curl commands for use in the terminal.

Check it out at: <https://www.assessment.gambitaccepted.com/api/v1/doc>

## Setting up the environment

### Cloning repository and installing dependencies

First you need to clone the project. Create a folder and run this command:
```sh
git clone https://V-Mann_Nick@bitbucket.org/V-Mann_Nick/assessment-backend.git
```

**For installing the dependencies there are two ways:**

#### 1. using pipenv

```sh
pipenv sync
```

#### 2. using pip and venv

```sh
python -m venv .venv
source .venv/bin/activate
pip install -r requirements.txt
```

To run, use:

```sh
pipenv run python api.py  # when using pipenv
# or
.venv/bin/python api.py
```

The api is avaiable under <http://localhost:5000/api/v1/doc>

### Installing and populating mongodb

The Api will not yet work. For the Api to have data, to read from, you're going
to need mongodb. Check with the
[docs](https://docs.mongodb.com/manual/installation/) of mongodb how to install
mongodb on your operating system

You can verify that it's running by using `systemctl status mongodb` (on linux)

Now you have two options. Either you populate the database on your own or you
import the collection I provide:

#### 1. Option: Populating database manually

You can populate the database from the shell:

```sh
export FLASK_APP=api.py
export FLASK_ENV=dev

pipenv run flask shell  # when using pipenv
# or
.venv/bin/flask shell
```

will bring you into a python shell. Now you can create mongodb documents by using:

```py
# Create a Funding object
funding = Funding(
  category="Games",
  location="United States",
  funding_amount = 555555,
  announced_date = "Okt 3 2015"
)
# save it to db
funding.save()
```

#### 2. Option: Import collection

For this step you're going to need [mongo-tools](https://github.com/mongodb/mongo-tools).

Now you can import the collection.

```sh
mongorestore mongodb_backup
```

___
**That's it. Enjoy!**
