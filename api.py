import os
import json

from app import create_app
from app.rest_api.funding.model import Funding


env = os.getenv("FLASK_ENV") or "dev"
print(f"Active environment: * {env} *")
app = create_app(env)


@app.shell_context_processor
def make_shell_context():
    return {"Funding": Funding}


if __name__ == "__main__":
    app.run()
