from flask import Flask, jsonify
from flask_restplus import Api
from flask_mongoengine import MongoEngine
from flask_cors import CORS


db: MongoEngine = MongoEngine()
api: Api = Api(
    title="REST Api for data on fundings",
    description="This Api provides data on the funding of various industries.",
    version="1.0",
    prefix="/api/v1",
    doc="/api/v1/doc",
)


def create_app(env=None):
    # avoid circular imports
    from app.config import config_by_name
    from app.routes import register_routes

    # initialize and integrate
    app = Flask(__name__)
    app.config.from_object(config_by_name[env or "dev"])
    if env == "dev":
        CORS(app)
    api.init_app(app)
    db.init_app(app)

    # register routes
    register_routes(api, app)

    # for testing
    @app.route("/health")
    def health():
        return jsonify("healthy")

    return app
