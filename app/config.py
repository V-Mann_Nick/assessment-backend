import os
import json


basedir = os.path.abspath(os.path.dirname(__file__))

# in production this file will exist
try:
    with open("/etc/funding_config.json") as config_file:
        config = json.load(config_file)
except:
    config = {}


class BaseConfig:
    CONFIG_NAME = "base"
    DEBUG = False
    SWAGGER_UI_OPERATION_ID = True
    SWAGGER_UI_DOC_EXPANSION = "list"
    SWAGGER_UI_REQUEST_DURATION = True
    MONGODB_DB = "funding"


class DevelopmentConfig(BaseConfig):
    CONFIG_NAME = "dev"
    SECRET_KEY = config.get("SECRET_KEY") or "secret-af"
    DEBUG = True
    TESTING = False


class TestingConfig(BaseConfig):
    CONFIG_NAME = "test"
    SECRET_KEY = os.getenv("TEST_SECRET_KEY", "Thanos did nothing wrong")
    DEBUG = True
    TESTING = True
    MONGODB_HOST = "mongomock://localhost"


class ProductionConfig(BaseConfig):
    CONFIG_NAME = "prod"
    SECRET_KEY = os.getenv("PROD_SECRET_KEY", "I'm Ron Burgundy?")
    DEBUG = False
    TESTING = False


EXPORT_CONFIGS = [
    DevelopmentConfig,
    TestingConfig,
    ProductionConfig,
]
config_by_name = {cfg.CONFIG_NAME: cfg for cfg in EXPORT_CONFIGS}
