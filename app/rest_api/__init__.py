def register_routes(api, app):
    from .funding.controller import ns as funding_api

    api.add_namespace(funding_api, path="/funding")
