from typing import List
from bson import ObjectId

from flask_restplus import Resource, Namespace, abort
from flask_restplus.fields import Integer, String
from flask_restplus.model import Model
from flask_accepts import responds

from .model import Funding
from .schema import FundingSchema
from .service import FundingService


ns: Namespace = Namespace("Funding")

# For Documentation
model: Model = ns.model(
    "Funding",
    {
        "id": String(description="universal unique identifier of length 24"),
        "category": String(description="the category of industry, which was funded"),
        "fundingAmount": Integer(description="how much the industry was funded"),
        "announcedDate": String(description="date, when funding was announced"),
    },
)


@ns.route("/")
class FundingResource(Resource):
    """endpoint for funding data"""

    @responds(schema=FundingSchema(many=True))
    @ns.doc("get all funding data")
    @ns.response(200, "Success", model)
    def get(self) -> List[Funding]:
        """Returns all funding data"""

        return FundingService.get_all()


@ns.route("/<string:id>")
@ns.doc(params={"id": "a string of length 24"})
class FundingIdResource(Resource):
    """endpoint for funding data by id"""

    @responds(schema=FundingSchema)
    @ns.doc("get funding data by id")
    @ns.response(200, "Success", model)
    @ns.response(400, "Bad Request")
    @ns.response(404, "Not Found")
    def get(self, id: str) -> Funding:
        """Returns funding data by id"""

        if len(id) != 24 or not ObjectId.is_valid(id):
            abort(
                400,
                f"'{id}' is not a valid ObjectId, must be a 24-character hex string",
            )

        result: Funding = FundingService.get_by_id(id)
        if result:
            return result
        else:
            abort(404, f"Funding resource with id '{id}' doesn't exist")
