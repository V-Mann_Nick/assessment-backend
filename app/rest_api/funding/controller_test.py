from unittest.mock import patch
from typing import List, Dict

from flask.testing import FlaskClient

from .service import FundingService
from app.test.fixtures import client, app
from .schema import FundingSchema


mock_data: List[Dict] = [
    {
        "id": "5ecbdcbfc3f981f4780c05c7",
        "category": "Beauty",
        "location": "United States",
        "fundingAmount": 36100,
        "announcedDate": "Aug 6, 2019",
    },
    {
        "id": "5ecbdcbfc3f981f4780c05c8",
        "category": "Beauty",
        "location": "United States",
        "fundingAmount": 87200,
        "announcedDate": "Jul 26, 2019",
    },
]


class TestFundingResource:
    endpoint: str = "/api/v1/funding/"

    # replace get_all function for testing
    @patch.object(
        FundingService, "get_all", lambda: mock_data,
    )
    def test_get(self, client: FlaskClient):
        """test GET method"""

        # check if expected data matches the result of the api
        expected = FundingSchema(many=True).dump(mock_data)
        results = client.get(self.endpoint, follow_redirects=True).get_json()
        for result in results:
            assert result in expected


class TestFundingIdResource:
    endpoint: str = "/api/v1/funding"

    # replace get_by_id function for testing
    @patch.object(
        FundingService,
        "get_by_id",
        # lambda function returns resource where id == id
        lambda id: [x for x in mock_data if x.get("id") == id][0],
    )
    def test_get(self, client: FlaskClient):
        """test GET method"""

        # check if expected data matches the result of the api
        expected = FundingSchema().dump(mock_data[0])
        result = client.get(
            f"{self.endpoint}/5ecbdcbfc3f981f4780c05c7", follow_redirects=True
        ).get_json()
        assert result == expected
