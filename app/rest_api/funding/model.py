from mongoengine import Document, StringField, IntField


class Funding(Document):
    """model definition for a MongoDB document Fundgin"""

    category = StringField(required=True, max_length=200)
    location = StringField(required=True, max_length=200)
    funding_amount = IntField(required=True)
    announced_date = StringField(required=True, max_length=200)

    def __repr__(self) -> str:
        return f"<{self.category} - {self.funding_amount}>"
