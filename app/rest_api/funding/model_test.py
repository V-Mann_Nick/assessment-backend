from pytest import fixture

from .model import Funding


@fixture
def funding() -> Funding:
    """create and return funding object"""

    return Funding(
        category="Games",
        location="Kosovo",
        funding_amount=55000,
        announced_date="Nov 22 2015",
    )


def test_funding_created(funding: Funding):
    """test if Funding object can be initialized"""

    assert funding
