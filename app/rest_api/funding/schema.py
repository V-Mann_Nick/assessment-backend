from marshmallow import Schema
from marshmallow.fields import String, Integer


class FundingSchema(Schema):
    """For serializing Funding data"""

    id = String()
    category = String()
    location = String()
    fundingAmount = Integer(attribute="funding_amount")
    announcedDate = String(attribute="announced_date")

    class Meta:
        ordered = True
