from pytest import fixture
from typing import Dict

from .schema import FundingSchema
from .model import Funding


@fixture
def schema() -> FundingSchema:
    """Initialize schema"""

    return FundingSchema()


@fixture
def funding() -> Funding:
    """Creates Funding object and commits it to db"""

    funding: Funding = Funding(
        category="Games",
        location="Kosovo",
        funding_amount=55000,
        announced_date="Nov 22 2015",
    )
    funding.save()
    return funding


def test_FundingSchema_create(schema: FundingSchema):
    """test if schema can be initialized"""

    assert schema


def test_FundingSchema_serialize(schema: FundingSchema, funding: Funding):
    """test if schema serializes correct"""

    # serialize
    result: Dict = schema.dump(funding)

    # check if result is as expected
    assert type(result.get("id")) == str
    assert result.get("category") == "Games"
    assert result.get("location") == "Kosovo"
    assert result.get("fundingAmount") == 55000
    assert result.get("announcedDate") == "Nov 22 2015"

    # Clear collection
    Funding.drop_collection()
