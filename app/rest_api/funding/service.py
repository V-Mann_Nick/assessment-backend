from typing import List

from .model import Funding


class FundingService:
    """This class provides an interface for the database"""

    @staticmethod
    def get_all() -> List[Funding]:
        """get all funding data"""

        return list(Funding.objects)

    @staticmethod
    def get_by_id(id: str) -> Funding:
        """get funding by id"""

        return Funding.objects(id=id).first()
