from pytest import fixture
from typing import List

from .model import Funding
from .service import FundingService


@fixture
def fundings() -> List[Funding]:
    """Creates two funding objects and commits them to db"""

    funding1: Funding = Funding(
        category="Games",
        location="Kosovo",
        funding_amount=55000,
        announced_date="Nov 22 2015",
    )
    funding1.save()

    funding2: Funding = Funding(
        category="Health",
        location="United States",
        funding_amount=555000,
        announced_date="Nov 22 2018",
    )
    funding2.save()

    return [funding1, funding2]


def test_get_all(fundings: List[Funding]):
    """test FundingService.get_all()"""

    # check if tested function gives expected result
    result: List[Funding] = FundingService.get_all()
    assert len(result) == 2

    # Clear collection
    Funding.drop_collection()


def test_get_by_id(fundings: List[Funding]):
    """test FundingService.get_by_id()"""

    # take one funding and get it's id as string
    a_funding: Funding = fundings[0]
    id: str = str(a_funding.id)  # a_funding.id is ObjectId

    # check if tested function gives expected result
    result: Funding = FundingService.get_by_id(id)
    assert result == a_funding

    # Clear collection
    Funding.drop_collection()
