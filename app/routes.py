def register_routes(api, app):
    from app.rest_api import register_routes as attach_api

    attach_api(api, app)
